package com.resource.management;

import com.resource.management.api.ResourceEndpoint;
import com.resource.management.core.ResourceConstants;
import com.resource.management.core.entities.activities.ResourceProvisionActivity;
import com.resource.management.core.entities.activities.ResourceProvisionActivity.*;
import com.resource.management.core.security.User;
import com.resource.management.core.security.auth.*;
import com.resource.management.db.*;
import com.uber.cadence.DomainAlreadyExistsError;
import com.uber.cadence.RegisterDomainRequest;
import com.uber.cadence.serviceclient.IWorkflowService;
import com.uber.cadence.serviceclient.WorkflowServiceTChannel;
import com.uber.cadence.worker.Worker;
import io.dropwizard.Application;
import io.dropwizard.auth.*;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resource Management Application class (used by the Dropwizard framework)
 * @author Vishwesh Tendolkar
 */
public class ResourceManagementApplication extends Application<ResourceManagementConfiguration> {
    private Logger logger = LoggerFactory.getLogger(ResourceManagementApplication.class);

    public static void main(final String[] args) throws Exception {
        new ResourceManagementApplication().run(args);
    }

    @Override
    public String getName() {
        return "ResourceManagement";
    }

    @Override
    public void initialize(final Bootstrap<ResourceManagementConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final ResourceManagementConfiguration configuration,
                    final Environment environment) {

        /* Add Managed cache/db objects to the environment */
        registerDBManager(environment);

        /* Add the REST api endpoint to the environment */
        registerResources(environment);

        /* Add the custom basic authenticator and authorizer to the environment */
        registerAuthClasses(environment);

        /* Register the cadence server domain */
        registerCadenceDomain();

        /* Start the cadence workflow listener */
        startCadenceProvisioningWorkflowListener();

    }

    /**
     * Register the REST API interfaces
     * @param environment
     */
    public void registerResources(final Environment environment){
        ResourceEndpoint resourceEndpoint = new ResourceEndpoint();
        environment.jersey().register(resourceEndpoint);
    }

    /**
     * Register the cache and db manager objects
     * @param environment
     */
    public void registerDBManager(final Environment environment){
        MongoConnectionManager mongoManager = new MongoConnectionManager();
        environment.lifecycle().manage(mongoManager);
        RedisConnectionManager redisManager = new RedisConnectionManager();
        environment.lifecycle().manage(redisManager);
    }

    /**
     * Register the basic authentication and authorizer classes
     * @param environment
     */
    public void registerAuthClasses(final Environment environment){
        environment.jersey().register(new AuthDynamicFeature(new BasicCredentialAuthFilter.Builder<User>()
                .setAuthenticator(new BasicAuthenticator())
                .setAuthorizer(new ResourceAuthorizer())
                .setRealm("auth-basic")
                .buildAuthFilter()));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
    }

    /**
     * Register the Cadence domain
     */
    public void registerCadenceDomain(){
        IWorkflowService cadenceService = new WorkflowServiceTChannel();
        RegisterDomainRequest request = new RegisterDomainRequest();
        request.setDescription("Resource Management Workflows");
        request.setEmitMetric(false);
        request.setName(ResourceConstants.CADENCE_DOMAIN);
        int retentionPeriodInDays = 1;
        request.setWorkflowExecutionRetentionPeriodInDays(retentionPeriodInDays);
        try {
            cadenceService.RegisterDomain(request);
            logger.info(
                    "Successfully registered domain \""
                            + ResourceConstants.CADENCE_DOMAIN
                            + "\" with retentionDays="
                            + retentionPeriodInDays);
        }
        catch (DomainAlreadyExistsError e) {
            logger.error("Domain \"" + ResourceConstants.CADENCE_DOMAIN + "\" is already registered");
        }
        catch(Exception e){
            logger.error("Unexpected error occurred while registering the Cadence domain", e);
        }
    }

    /**
     * Start the cadence workflow listener. This will start listening to workflow and activity task lists
     */
    public void startCadenceProvisioningWorkflowListener() {
        Worker.Factory factory = new Worker.Factory(ResourceConstants.CADENCE_DOMAIN);
        Worker worker = factory.newWorker(ResourceProvisionActivity.TASK_LIST);
        worker.registerWorkflowImplementationTypes(ProvisioningWorkflowImpl.class);
        worker.registerActivitiesImplementations(new ProvisionActivityImpl());

        /* Start listening to provisioning requests */
        factory.start();
    }
}
