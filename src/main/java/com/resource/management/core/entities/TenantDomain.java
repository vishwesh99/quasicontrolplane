package com.resource.management.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import javax.validation.constraints.NotNull;

/**
 * Tenant Domain POJO
 * @author Vishwesh Tendolkar
 */

@Entity
public class TenantDomain {
    @Id
    @Property
    private ObjectId id;
    @NotNull
    private String name;
    @NotNull
    private String location;
    private boolean isMetered = false;

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    @JsonIgnore
    public Boolean getMetered() {
        return isMetered;
    }

    @JsonProperty
    @JsonIgnore
    public void setMetered(Boolean metered) {
        isMetered = metered;
    }

    @JsonProperty
    public String getLocation() {
        return location;
    }

    @JsonProperty
    public void setLocation(String location) {
        this.location = location;
    }
}
