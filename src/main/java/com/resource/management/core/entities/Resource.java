package com.resource.management.core.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import javax.validation.constraints.NotNull;

/**
 * Resource POJO
 * @author Vishwesh Tendolkar
 */

@Entity
public class Resource {
    @Id
    private ObjectId id;
    @NotNull
    private String name;
    @NotNull
    private String type;
    @Embedded
    private TenantDomain tenantDomain;

    public Resource(){
        /* No args constructor */
    }

    public Resource(String name, String type, TenantDomain tenantDomain){
        this.name = name;
        this.type = type;
        this.tenantDomain = tenantDomain;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getType() {
        return type;
    }

    @JsonProperty
    public void setType(String type) {
        this.type = type;
    }

    public TenantDomain getTenantDomain() {
        return tenantDomain;
    }

    public void setTenantDomain(TenantDomain tenantDomain) {
        this.tenantDomain = tenantDomain;
    }

}