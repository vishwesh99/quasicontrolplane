package com.resource.management.core.entities.activities;

import com.resource.management.core.ResourceService;
import com.resource.management.core.entities.Resource;
import com.uber.cadence.activity.ActivityMethod;
import com.uber.cadence.activity.ActivityOptions;
import com.uber.cadence.common.RetryOptions;
import com.uber.cadence.workflow.Workflow;
import com.uber.cadence.workflow.WorkflowMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

/**
 * Workflow and Activity implementation for Resource provisioning
 * @author Vishwesh Tendolkar
 */

public class ResourceProvisionActivity {

    private Logger logger = LoggerFactory.getLogger(ResourceProvisionActivity.class);
    public static final String TASK_LIST = "ResourceProvisionActivity";

    /* Workflow stub */
    public interface ProvisioningWorkflow {
        @WorkflowMethod(executionStartToCloseTimeoutSeconds = 10, taskList = TASK_LIST)
        String provisionResource(Resource resource);
    }

    /* Activity stub */
    public interface ProvisioningActivity{
        @ActivityMethod(scheduleToCloseTimeoutSeconds = 2)
        String saveResourceToMetadata(Resource resource);

        @ActivityMethod()
        String createResource(Resource resource);
    }

    /* Activity Implementation */
    public static class ProvisionActivityImpl implements ProvisioningActivity{
        @Override
        public String saveResourceToMetadata(Resource resource){
            ResourceService.addResource(resource);
            return resource.getName();
        }

        @Override
        public String createResource(Resource resource){

            /* This will have resource-specific provisioning steps, where the actual resource will
             * get provisioned
             */

            /* Returning no-op since this is just a sample resource */
            return "no-op";
        }

    }

    /* Worflow Implementation */
    public static class ProvisioningWorkflowImpl implements ProvisioningWorkflow{
        private final ProvisioningActivity activities = Workflow.newActivityStub(
                                                            ProvisioningActivity.class,
                                                            new ActivityOptions.Builder()
                                                                    .setScheduleToCloseTimeout(Duration.ofSeconds(30)) //In actual prov request, timeout should be more
                                                                    .setRetryOptions(
                                                                            new RetryOptions.Builder()
                                                                                    .setInitialInterval(Duration.ofSeconds(5))
                                                                                    .setExpiration(Duration.ofSeconds(5))
                                                                                    .setDoNotRetry(IllegalArgumentException.class)
                                                                                    .build())
                                                                    .build());

        @Override
        public String provisionResource(Resource resource){
            String resourceName = activities.saveResourceToMetadata(resource);
            activities.createResource(resource);

            return resourceName;
        }
    }
}
