package com.resource.management.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

/**
 * Utility Methods
 * @author Vishwesh Tendolkar
 */

public class ResourceUtil {
    private Logger logger = LoggerFactory.getLogger(ResourceUtil.class);

    /**
     * Builds an HTTP response given the HTTP status, message and the message type
     * @param status
     * @param message
     * @param type
     * @return
     */
    public static Response buildResponse(Response.Status status, String message, String type){
        Response.ResponseBuilder builder = Response.status(status);
        builder.entity(message);
        builder.type(type);

        return builder.build();
    }
}
