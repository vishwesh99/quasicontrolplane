package com.resource.management.core.security.auth;

import com.resource.management.core.security.User;
import io.dropwizard.auth.Authorizer;

/**
 * Resource Management Basic Authorizer (should be plugged in to IDM/LDAP systems)
 * @author Vishwesh Tendolkar
 */
public class ResourceAuthorizer implements Authorizer<User> {

    @Override
    public boolean authorize(User user, String role) {

        /* Check if the user has the provided role */
        return user.getRoles() != null && user.getRoles().contains(role);
    }
}
