package com.resource.management.core.security.auth;

import com.resource.management.core.security.User;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

/**
 * Resource Management Basic Authenticator (should be plugged in to IDM/LDAP systems)
 * @author Vishwesh Tendolkar
 */

public class BasicAuthenticator implements Authenticator<BasicCredentials, User> {
    /* Map of users and roles */
    private static final Map<String, Set<String>> VALID_USERS = ImmutableMap.of(
            "guest", ImmutableSet.of(),
            "user", ImmutableSet.of("USER"),
            "admin", ImmutableSet.of("ADMIN", "USER")
    );

    @Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException
    {
        /* Same auth code. Should be replaced by other auth schemes like LDAPAutheiticator etc */
        if (VALID_USERS.containsKey(credentials.getUsername()) && "password".equals(credentials.getPassword()))
            return Optional.of(new User(credentials.getUsername(), VALID_USERS.get(credentials.getUsername())));

        return Optional.empty();
    }
}
