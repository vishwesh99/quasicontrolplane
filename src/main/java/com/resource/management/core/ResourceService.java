package com.resource.management.core;

import com.resource.management.core.entities.Resource;
import com.resource.management.core.entities.TenantDomain;
import com.resource.management.db.MongoConnection;
import com.resource.management.db.RedisConnection;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Model APIs to support interaction with Resource and Tenant Domain
 * @author Vishwesh Tendolkar
 */

public class ResourceService {
    private static Logger logger = LoggerFactory.getLogger(ResourceService.class);

    /**************************************** TENANT DOMAIN *********************************************/

    /**
     * Find a tenant given the tenant name
     * @param tenantDomainName
     * @return Tenant Domain
     */
    public static TenantDomain getTenantDomain(String tenantDomainName){
        if(tenantDomainName == null || tenantDomainName.isEmpty())
            return null;

        /* Check if the cache contains the required tenant domain */
        RedissonClient redisClient = RedisConnection.getClient();
        RMapCache<String, TenantDomain> cache = redisClient.getMapCache(ResourceConstants.TENANT_KEY);
        if(cache != null && !cache.isEmpty())
            if(cache.containsKey(tenantDomainName)) {
                logger.debug("Found tenant domain: " + tenantDomainName + " in the cache. Retrieving.");
                return cache.get(tenantDomainName);
            }


        /* Query and fetch the tenant domain */
        Datastore datastore = MongoConnection.getDataStore();
        TenantDomain tenantDomain = datastore.createQuery(TenantDomain.class).filter("name == ", tenantDomainName).get();
        if(tenantDomain != null) {
            logger.debug("Found tenant domain: " + tenantDomainName + " in the database. Retrieving.");

            /* Add the tenant domain to the cache */
            cache.put(tenantDomainName, tenantDomain);

            return tenantDomain;
        }

        /* Tenant not found */
        return null;
    }

    /**
     * Get a list of all the tenant domains
     * @return
     */
    public static List<TenantDomain> getTenantDomains(){

        /* Get a list of all the tenant domains */
        Datastore datastore = MongoConnection.getDataStore();
        List<TenantDomain> tenantDomains = datastore.createQuery(TenantDomain.class).asList();
        if(tenantDomains != null) {
            logger.debug("Found tenant domains: " + tenantDomains + " in the database. Retrieving.");
            return tenantDomains;
        }

        /* Tenant not found */
        logger.debug("Found no tenant domains in the database. Returning an empty list.");
        return new ArrayList<TenantDomain>();
    }

    /**
     * Add a tenant domain to the metadata
     * @param tenantDomain
     */
    public static void addTenantDomain(TenantDomain tenantDomain){
        if(tenantDomain == null)
            return;

        /* Add the tenant domain to the database */
        Datastore datastore = MongoConnection.getDataStore();
        Key<TenantDomain> key = datastore.save(tenantDomain);
        if(key == null || key.getId() == null) {
            logger.error("Unable to add tenant domain: " + tenantDomain + " to the metadata");
            return;
        }

        logger.debug("Added tenant domain: " + tenantDomain.getName() + " in the database.");

        /* Add the tenant domain to the cache, only if the db insert is successful */
        /* Not adding a timeout to the tenant domain, since tenant domain wont be as volatile as a resource */
        RedissonClient redisClient = RedisConnection.getClient();
        RMapCache<String, TenantDomain> cache = redisClient.getMapCache(ResourceConstants.TENANT_KEY);
        cache.put(tenantDomain.getName(), tenantDomain);
        logger.debug("Added tenant domain: " + tenantDomain.getName() + " in the cache.");
    }






    /**************************************** RESOURCE *********************************************/

    /**
     * Find a resource given the resource name
     * @param resourceName
     * @return Resource
     */
    public static Resource getResource(String resourceName, String tenantDomainName){
        if(resourceName == null || resourceName.isEmpty() || tenantDomainName == null || tenantDomainName.isEmpty())
            return null;

        /* Check if the cache contains the required resource */
        RedissonClient redisClient = RedisConnection.getClient();
        RMapCache<String, Resource> cache = redisClient.getMapCache(ResourceConstants.RESOURCE_KEY);
        if(cache != null && !cache.isEmpty())
            if(cache.containsKey(resourceName + ":" + tenantDomainName)) {
                logger.debug("Found resource: " + resourceName + " in tenant domain " + tenantDomainName + " in the cache. Retrieving.");
                return cache.get(resourceName + ":" + tenantDomainName);
            }


        /* Query and fetch the resource */
        Datastore datastore = MongoConnection.getDataStore();
        Query<Resource> query = datastore.createQuery(Resource.class);
        query.and(
                query.criteria("name").equal(resourceName),
                query.criteria("tenantDomain.name").equal(tenantDomainName)
        );
        Resource resource = query.get();
        if(resource != null) {
            logger.debug("Found resource: " + resourceName + " in tenant domain " + tenantDomainName + " in the database. Retrieving.");
            return resource;
        }

        /* Tenant not found */
        return null;
    }

    /**
     * Get a list of all the resources in a tenant domain
     * @return
     */
    public static List<Resource> getResources(String tenantDomainName){

        /* Get a list of all the resources */
        Datastore datastore = MongoConnection.getDataStore();
        List<Resource> resources = datastore.createQuery(Resource.class)
                                            .field("tenantDomain.name").equal(tenantDomainName)
                                            .asList();

        if(resources != null) {
            logger.debug("Found resources: " + resources + " in tenant domain " + tenantDomainName + " in the database. Retrieving.");
            return resources;
        }

        /* Resources not found */
        return new ArrayList<Resource>();
    }

    /**
     * Add a resource to a tenant domain
     * @param resource
     */
    public static void addResource(Resource resource){
        if(resource == null)
            return;

        /* Add the resource to the database */
        Datastore datastore = MongoConnection.getDataStore();
        Key<Resource> key = datastore.save(resource);
        if(key == null || key.getId() == null) {
            logger.error("Unable to add resource: " + resource + " to the metadata");
            return;
        }

        logger.debug("Added resource: " + resource.getName() + " in the database.");

        /* Add the resource to the cache, only if the db insert is successful */
        /* Add a cache timeout of 1 day to the resource */
        RedissonClient redisClient = RedisConnection.getClient();
        RMapCache<String, Resource> cache = redisClient.getMapCache(ResourceConstants.RESOURCE_KEY);
        cache.put(resource.getName(), resource, 1, TimeUnit.DAYS);
        logger.debug("Added resource: " + resource.getName() + " in the cache.");
    }
}
