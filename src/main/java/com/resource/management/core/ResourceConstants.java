package com.resource.management.core;

/**
 * Constants
 * @author Vishwesh Tendolkar
 */
public class ResourceConstants {

    /* Cache + DB */
    public static final String MONGO_DATABASE_NAME = "resourceMgmt";
    public static final String RESOURCE_KEY = "resource";
    public static final String TENANT_KEY = "tenantDomain";
    public static final String ENTITY_PACKAGE = "com.resource.management.core.entities";

    /* Cadence */
    public static final String CADENCE_DOMAIN = "resources";
}
