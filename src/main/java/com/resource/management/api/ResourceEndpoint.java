package com.resource.management.api;

import com.google.gson.Gson;

import com.mongodb.*;
import com.resource.management.core.ResourceConstants;
import com.resource.management.core.ResourceService;
import com.resource.management.core.ResourceUtil;
import com.resource.management.core.entities.Resource;
import com.resource.management.core.entities.TenantDomain;
import com.resource.management.core.entities.activities.ResourceProvisionActivity;
import com.resource.management.core.entities.activities.ResourceProvisionActivity.*;
import com.resource.management.core.security.User;
import com.resource.management.db.MongoConnection;
import com.resource.management.db.RedisConnection;
import com.uber.cadence.WorkflowExecution;
import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowOptions;
import io.dropwizard.auth.Auth;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.Duration;
import java.util.List;

/**
 * This REST endpoint takes care of handling CRUD operations for a resource under a tenancy domain and a tenancy domain itself
 * @author Vishwesh Tendolkar
 */

@Path("/api/v1.1/domain")
@Produces(MediaType.APPLICATION_JSON)
public class ResourceEndpoint {

    private Logger logger = LoggerFactory.getLogger(ResourceEndpoint.class);
    private MongoClient mongoClient = MongoConnection.getClient();
    private RedissonClient redisClient = RedisConnection.getClient();

    @Path("/{tenantDomain}/resources")
    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getResources(@PathParam("tenantDomain") String tenantDomainName,
                                 @Context HttpServletRequest request,
                                 @Auth User user){


        /* Check if tenant domain is valid */
        if(tenantDomainName == null || tenantDomainName.isEmpty())
            return Response.status(Response.Status.NOT_FOUND).build();

        /* Retrieve the tenant domain */
        TenantDomain tenantDomain = ResourceService.getTenantDomain(tenantDomainName);
        if(tenantDomain == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        /* Get the resources for the tenant domain */
        List<Resource> resources = ResourceService.getResources(tenantDomainName);

        /* Build the result */
        Gson gson = new Gson();
        return Response.ok(gson.toJson(resources)).build();
    }

    @Path("/{tenantDomain}/resources")
    @POST
    @RolesAllowed({ "USER" })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addResource(@PathParam("tenantDomain") String tenantDomainName,
                                @Context HttpServletRequest request,
                                Resource resource,
                                @Auth User user){

        /* Check if a json input is sent */
        if(resource == null)
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST, "Please provide a json input", MediaType.TEXT_PLAIN);

        /* Validate the input */
        if(resource.getName() == null || resource.getName().isEmpty() || resource.getType() == null || resource.getType().isEmpty())
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                                     "Please provide a valid name and type for the resource",
                                              MediaType.TEXT_PLAIN);

        if(!resource.getName().matches("^[a-zA-Z][a-zA-Z0-9-]{1,23}[a-zA-Z]") || !resource.getName().matches("^[a-zA-Z][a-zA-Z0-9-]{1,23}[a-zA-Z]"))
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                    "Please provide a valid name and type for the resource. Both name and type must start with a letter, followed by letters, numbers or \"-\". The maximum length is 25 characters",
                              MediaType.TEXT_PLAIN);

        TenantDomain tenantDomain = ResourceService.getTenantDomain(tenantDomainName);
        if(tenantDomain == null)
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                                     "Please provide a valid tenant domain",
                                              MediaType.TEXT_PLAIN);
        Resource existingResource = ResourceService.getResource(resource.getName(), tenantDomainName);
        if(existingResource != null)
            ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                             "A resource with name: " + resource.getName() + " already exists in the tenant domain " + tenantDomainName + ". Please provide unique details for the resource",
                                      MediaType.TEXT_PLAIN);

        /* Add the resource */
        resource.setTenantDomain(tenantDomain);

        /* Sync call */
        /*
        ResourceService.addResource(resource);
        */


        /* Async call */
        WorkflowClient workflowClient = WorkflowClient.newInstance(ResourceConstants.CADENCE_DOMAIN);
        WorkflowOptions workflowOptions =
                new WorkflowOptions.Builder()
                        .setTaskList(ResourceProvisionActivity.TASK_LIST)
                        .setExecutionStartToCloseTimeout(Duration.ofSeconds(30)) //In actual prov request, timeout should be more
                        .build();
        ProvisioningWorkflow workflow = workflowClient.newWorkflowStub(ProvisioningWorkflow.class, workflowOptions);
        WorkflowExecution workflowExecution = WorkflowClient.start(workflow::provisionResource, resource);

        /* Return result */
        return ResourceUtil.buildResponse(Response.Status.ACCEPTED,
                "Resource " + resource.getName() + " of type " + resource.getType() + " is being created under tenant domain " + tenantDomainName +
                " with workflowId = " + workflowExecution.getWorkflowId() + " and runId = " + workflowExecution.getRunId(),
                MediaType.TEXT_PLAIN);
    }

    @Path("/{tenantDomain}")
    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTenantDomain(@PathParam("tenantDomain") String tenantDomainName,
                                    @Context HttpServletRequest request,
                                    @Auth User user){


        /* Check if tenant domain is valid */
        if(tenantDomainName == null || tenantDomainName.isEmpty())
            return Response.status(Response.Status.NOT_FOUND).build();

        /* Retrieve the tenant domain */
        TenantDomain tenantDomain = ResourceService.getTenantDomain(tenantDomainName);
        if(tenantDomain == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        Gson gson = new Gson();
        return Response.ok(gson.toJson(tenantDomain)).build();
    }


    @POST
    @RolesAllowed({ "ADMIN" })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTenantDomain(@Context HttpServletRequest request,
                                    TenantDomain tenantDomain,
                                    @Auth User user){

        /* Check if a json input is sent */
        if(tenantDomain == null)
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST, "Please provide a valid tenant domain name and location", MediaType.TEXT_PLAIN);

        /* Validate the input */
        if(tenantDomain.getName() == null || tenantDomain.getName().isEmpty() || tenantDomain.getLocation() == null || tenantDomain.getLocation().isEmpty())
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                    "Please provide a valid name and location for the tenant domain",
                    MediaType.TEXT_PLAIN);

        if(!tenantDomain.getName().matches("^[a-zA-Z][a-zA-Z0-9-]{1,23}[a-zA-Z]") || !tenantDomain.getName().matches("^[a-zA-Z][a-zA-Z0-9-]{1,23}[a-zA-Z]"))
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                    "Please provide a valid name and location for the tenant domain. Both name and location must start with a letter, followed by letters, numbers or \"-\". The maximum length is 25 characters",
                    MediaType.TEXT_PLAIN);

        TenantDomain existingTenantDomain = ResourceService.getTenantDomain(tenantDomain.getName());
        if(existingTenantDomain != null)
            return ResourceUtil.buildResponse(Response.Status.BAD_REQUEST,
                    "A tenant domain with name: " + tenantDomain.getName() + " already exists. Please provide unique details for the tenant domain",
                    MediaType.TEXT_PLAIN);

        /* Add the tenant domain */
        ResourceService.addTenantDomain(tenantDomain);
        return ResourceUtil.buildResponse(Response.Status.OK,
                "Tenant Domain " + tenantDomain.getName() + " in location " + tenantDomain.getLocation() + " has been created",
                MediaType.TEXT_PLAIN);
    }

}
