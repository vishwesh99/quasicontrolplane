package com.resource.management;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Resource Management Configuration class (used by the Dropwizard framework)
 * @author Vishwesh Tendolkar
 */

public class ResourceManagementConfiguration extends Configuration {
    /*
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();
    */

    @NotNull
    private Map<String, String> redisConfig = new HashMap<>();

    @Valid
    @NotNull
    private JerseyClientConfiguration jerseyClient = new JerseyClientConfiguration();

    /*
    @JsonProperty
    public DataSourceFactory getDatabase() {
        return database;
    }

    @JsonProperty
    public void setDatabase(DataSourceFactory database) {
        this.database = database;
    }
    */

    @JsonProperty("redis")
    public Map<String, String> getRedisConfig() {
        return redisConfig;
    }

    @JsonProperty("redis")
    public void setRedisConfig(Map<String, String> redisConfig) {
        this.redisConfig = redisConfig;
    }

    @JsonProperty("jerseyClient")
    public JerseyClientConfiguration getJerseyClient() {
        return jerseyClient;
    }

    @JsonProperty("jerseyClient")
    public void setJerseyClient(JerseyClientConfiguration jerseyClient) {
        this.jerseyClient = jerseyClient;
    }



}
