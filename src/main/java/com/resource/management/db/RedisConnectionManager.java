package com.resource.management.db;

import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Redis Connection Manager
 * @author Vishwesh Tendolkar
 */

public class RedisConnectionManager implements Managed {
    private Logger logger = LoggerFactory.getLogger(MongoConnectionManager.class);

    @Override
    public void start() throws Exception{
        /* Don't do anything */
    }

    @Override
    public void stop() throws Exception{
        /* Terminate the client */
        logger.info("Terminating the Redis client");
        RedisConnection.getClient().shutdown();
    }
}
