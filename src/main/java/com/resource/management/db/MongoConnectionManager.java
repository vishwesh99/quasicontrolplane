package com.resource.management.db;

import com.resource.management.ResourceManagementApplication;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MongoDB Connection Manager
 * @author Vishwesh Tendolkar
 */

public class MongoConnectionManager implements Managed {
    private Logger logger = LoggerFactory.getLogger(MongoConnectionManager.class);

    @Override
    public void start() throws Exception{
        /* Don't do anything */
    }

    @Override
    public void stop() throws Exception{
        /* Terminate the client */
        logger.info("Terminating the MondoDB client");
        MongoConnection.getClient().close();
    }
}
