package com.resource.management.db;

import com.mongodb.MongoClient;
import com.resource.management.core.ResourceConstants;
import net.bytebuddy.implementation.bind.annotation.Morph;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * MongoDB connector
 * @author Vishwesh Tendolkar
 */

public class MongoConnection {
    private static Logger logger = LoggerFactory.getLogger(MongoConnection.class);
    private static Properties props;
    private static MongoClient mongoClient;
    private static Datastore datastore;

    static{
        /* Load the system properties */
        props = new Properties();
        try {
            InputStream input = new FileInputStream("config.properties");
            props.load(input);
        }
        catch(Exception e){
            logger.error("Unable to load system properties", e);
        }

        /* Prepare the mongo client */
        try {
            mongoClient = new MongoClient((props.getProperty("mongodbhost")), Integer.valueOf(props.getProperty("mongodbport")));

        }
        catch(Exception e){
            logger.error("Unable to load Mongo DB client", e);
        }

        /* Prepare the morphia datastore */
        try {
            Morphia morphia = new Morphia();
            morphia.mapPackage(ResourceConstants.ENTITY_PACKAGE);
            datastore = morphia.createDatastore(mongoClient, ResourceConstants.MONGO_DATABASE_NAME);
            datastore.ensureIndexes();
        }
        catch(Exception e){
            logger.error("Unable to load Morphia Datastore", e);
        }
    }

    /* Private constructor */
    private MongoConnection(){

    }

    /* Private static class with the singleton client */
    private static class MongoClientHelper{
        private static final MongoConnection CLIENT = new MongoConnection();
    }

    /* Static method to return mongo client */
    public static MongoClient getClient(){
        return MongoClientHelper.CLIENT.mongoClient;
    }

    /* Static method to return morphia datastore */
    public static Datastore getDataStore(){ return MongoClientHelper.CLIENT.datastore; }
}
