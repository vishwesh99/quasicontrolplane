package com.resource.management.db;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Redis Connector
 * @author Vishwesh Tendolkar
 */

public class RedisConnection {
    private static Logger logger = LoggerFactory.getLogger(RedisConnection.class);
    private static Properties props;
    private static RedissonClient redisClient;

    static{
        /* Load the system properties */
        props = new Properties();
        try {
            InputStream input = new FileInputStream("config.properties");
            props.load(input);
        }
        catch(Exception e){
            logger.error("Unable to load system properties", e);
        }

        /* Prepare the mongo client */
        try {
            logger.info("redis host : " + props.getProperty("redishost"));
            logger.info("redis port : " + props.getProperty("redisport"));
            Config config = new Config();
            config.useSingleServer().setAddress("redis://" + props.getProperty("redishost") + ":" + props.getProperty("redisport"));
            redisClient = Redisson.create(config);
        }
        catch(Exception e){
            logger.error("Unable to load Redis client", e);
        }
    }

    /* Private constructor */
    private RedisConnection(){

    }

    /* Private static class with the singleton client */
    private static class RedisClientHelper{
        private static final RedisConnection CLIENT = new RedisConnection();
    }

    /* Static method to return singleton */
    public static RedissonClient getClient(){
        return RedisClientHelper.CLIENT.redisClient;
    }
}
